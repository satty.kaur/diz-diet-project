import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FoodItemsService} from '../FoodData/food-items.service';

@Component({
  selector: 'app-food-dialog',
  templateUrl: './food-dialog.component.html',
  styleUrls: ['./food-dialog.component.css']
})
export class FoodDialogComponent implements OnInit {

   food;
   removedFoods: any [];

  constructor(public dialogRef: MatDialogRef<FoodDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public foodItem: any,
              public foodData: FoodItemsService) { }

  ngOnInit(): void {
     if (this.foodItem.macro === 'Pro') {
       this.food = this.foodData.proFoods.filter(item => item.id === this.foodItem.id)[0];
     } else if (this.foodItem.macro === 'Carb') {
       this.food = this.foodData.carbFoods.filter(item => item.id === this.foodItem.id)[0];
     } else if (this.foodItem.macro === 'Fat') {
       this.food = this.foodData.fatFoods.filter(item => item.id === this.foodItem.id)[0];
     }
  }


  macroTitle() {
    if (this.foodItem.macro === 'Pro') {
      return {color: '#29ABE2'};
    } else if (this.foodItem.macro === 'Carb') {
      return {color: '#009e2a'};
    } else {
      return {color: '#e39a12'};
    }
  }


  removeFood(id: number) {
    console.log('you removed ' + this.food.name);
  }

}
