import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {RoutingModule} from './routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { GenderComponent } from './UserDetails/gender/gender.component';
import { SplashScreenComponent } from './splash-screen/splash-screen.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatNativeDateModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatButtonModule} from '@angular/material/button';
import {MatTabsModule} from '@angular/material/tabs';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatIconModule} from '@angular/material/icon';
import {ScrollingModule} from '@angular/cdk/scrolling';
import { AgeComponent } from './UserDetails/age/age.component';
import { HeightComponent } from './UserDetails/height/height.component';
import { WeightComponent } from './UserDetails/weight/weight.component';
import { BodyTypeComponent } from './UserDetails/body-type/body-type.component';
import { DietPreferenceComponent } from './UserDetails/diet-preference/diet-preference.component';
import { GoalComponent } from './UserDetails/goal/goal.component';
import { UserDetailsComponent } from './UserDetails/user-details/user-details.component';
import {MatCardModule} from '@angular/material/card';
import { DietPlansComponent } from './diet-plans/diet-plans.component';
import {MatDialogModule} from '@angular/material/dialog';
import { FoodDialogComponent } from './food-dialog/food-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    GenderComponent,
    SplashScreenComponent,
    AgeComponent,
    HeightComponent,
    WeightComponent,
    BodyTypeComponent,
    DietPreferenceComponent,
    GoalComponent,
    UserDetailsComponent,
    DietPlansComponent,
    FoodDialogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    RoutingModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatButtonModule,
    FormsModule,
    MatTabsModule,
    MatButtonToggleModule,
    MatIconModule,
    ScrollingModule,
    MatCardModule,
    MatDialogModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
