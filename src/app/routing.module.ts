import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {SplashScreenComponent} from './splash-screen/splash-screen.component';
import {UserDetailsComponent} from './UserDetails/user-details/user-details.component';
import {DietPlansComponent} from './diet-plans/diet-plans.component';


@NgModule({
  imports: [
    RouterModule.forRoot([
      {path: 'home', component: SplashScreenComponent},
      {path: '', redirectTo: 'home', pathMatch: 'full'},
      {path: 'user-details', component: UserDetailsComponent},
      {path: 'diet-plans', component: DietPlansComponent}
    ])
  ],
  exports: [
    RouterModule
  ]
})

export class RoutingModule {

}
