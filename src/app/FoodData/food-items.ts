

export interface ProItems {
  id: number;
  name: string;
  imageUrl: string;
}

export interface CarbItems {
  id: number;
  name: string;
  imageUrl: string;
}

export interface FatItems {
  id: number;
  name: string;
  imageUrl: string;
}
