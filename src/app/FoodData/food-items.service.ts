import { Injectable } from '@angular/core';
import {CarbItems, FatItems, ProItems} from './food-items';

@Injectable({
  providedIn: 'root'
})
export class FoodItemsService {


  proFoods: ProItems [] = [
    {
      id: 1,
      name: 'Bacon',
      imageUrl: '../../../assets/Images/pro/bacon.png'
    },
    {
      id: 2,
      name: 'Beef',
      imageUrl: '../../../assets/Images/pro/beef.png'
    },
    {
      id: 3,
      name: 'Beef Mince',
      imageUrl: '../../../assets/Images/pro/beefmince.png'
    },
    {
      id: 4,
      name: 'Blue Cheese',
      imageUrl: '../../../assets/Images/pro/cheeseblue.png'
    },
    {
      id: 5,
      name: 'Camembert Cheese',
      imageUrl: '../../../assets/Images/pro/cheesecamembert.png'
    },
    {
      id: 6,
      name: 'Cottage Cheese',
      imageUrl: '../../../assets/Images/pro/cheesecottage.png'
    },
    {
      id: 7,
      name: 'Feta Cheese',
      imageUrl: '../../../assets/Images/pro/cheesefeta.png'
    },
    {
      id: 8,
      name: 'Goats Cheese',
      imageUrl: '../../../assets/Images/pro/cheesegoat.png'
    },
    {
      id: 9,
      name: 'Gruyere Cheese',
      imageUrl: '../../../assets/Images/pro/cheesegruyere.png'
    },
    {
      id: 10,
      name: 'Mozzarella Cheese',
      imageUrl: '../../../assets/Images/pro/cheesemozzarella.png'
    }
    ,
    {
      id: 11,
      name: 'Parmesan Cheese',
      imageUrl: '../../../assets/Images/pro/cheeseparmesan.png'
    },
    {
      id: 12,
      name: 'Swiss Cheese',
      imageUrl: '../../../assets/Images/pro/cheeseswiss.png'
    },
    {
      id: 13,
      name: 'Chicken Breast',
      imageUrl: '../../../assets/Images/pro/chickenbreast.png'
    },
    {
      id: 14,
      name: 'Eggs',
      imageUrl: '../../../assets/Images/pro/eggs.png'
    },
    {
      id: 15,
      name: 'Anchovies',
      imageUrl: '../../../assets/Images/pro/fishanchovy.png'
    },
    {
      id: 16,
      name: 'Bass',
      imageUrl: '../../../assets/Images/pro/fishbass.png'
    },
    {
      id: 17,
      name: 'Cod',
      imageUrl: '../../../assets/Images/pro/fishcod.png'
    },
    {
      id: 18,
      name: 'Haddock',
      imageUrl: '../../../assets/Images/pro/fishhaddock.png'
    },
    {
      id: 19,
      name: 'Halibut',
      imageUrl: '../../../assets/Images/pro/fishhalibut.png'
    },
    {
      id: 20,
      name: 'Herring',
      imageUrl: '../../../assets/Images/pro/fishherring.png'
    },
    {
      id: 21,
      name: 'Monkfish',
      imageUrl: '../../../assets/Images/pro/fishmonkfish.png'
    },
    {
      id: 21,
      name: 'Pollock',
      imageUrl: '../../../assets/Images/pro/fishpollock.png'
    },
    {
      id: 22,
      name: 'Roe',
      imageUrl: '../../../assets/Images/pro/fishroe.png'
    },
    {
      id: 23,
      name: 'Salmon',
      imageUrl: '../../../assets/Images/pro/fishsalmon.png'
    },
    {
      id: 24,
      name: 'Tuna',
      imageUrl: '../../../assets/Images/pro/fishtuna.png'
    },
    {
      id: 25,
      name: 'Lamb',
      imageUrl: '../../../assets/Images/pro/lamb.png'
    },
    {
      id: 26,
      name: 'Tempeh',
      imageUrl: '../../../assets/Images/pro/tempeh.png'
    },
    {
      id: 27,
      name: 'Tofu',
      imageUrl: '../../../assets/Images/pro/tofu.png'
    },
    {
      id: 28,
      name: 'Turkey Breast',
      imageUrl: '../../../assets/Images/pro/turkeybreast.png'
    },
    {
      id: 29,
      name: 'Turkey Mince',
      imageUrl: '../../../assets/Images/pro/turkeymince.png'
    },
    {
      id: 30,
      name: 'Greek Yogurt',
      imageUrl: '../../../assets/Images/pro/yogurtgreek.png'
    },
    {
      id: 31,
      name: 'Low Fat Yogurt',
      imageUrl: '../../../assets/Images/pro/yogurtlfat.png'
    }
  ];

  carbFoods: CarbItems [] = [
    {
      id: 1,
      name: 'Apple',
      imageUrl: '../../../assets/Images/carb/apple.png'
    },
    {
      id: 2,
      name: 'Apricots',
      imageUrl: '../../../assets/Images/carb/apricots.png'
    },
    {
      id: 3,
      name: 'Artichoke',
      imageUrl: '../../../assets/Images/carb/artichoke.png'
    },
    {
      id: 4,
      name: 'Asparagus',
      imageUrl: '../../../assets/Images/carb/asparagus.png'
    },
    {
      id: 5,
      name: 'Aubergine',
      imageUrl: '../../../assets/Images/carb/aubergine.png'
    },
    {
      id: 6,
      name: 'Baked Beans',
      imageUrl: '../../../assets/Images/carb/bakedbeans.png'
    },
    {
      id: 7,
      name: 'Bananas',
      imageUrl: '../../../assets/Images/carb/bananas.png'
    },
    {
      id: 8,
      name: 'Beetroot',
      imageUrl: '../../../assets/Images/carb/beetroot.png'
    },
    {
      id: 9,
      name: 'Blueberries',
      imageUrl: '../../../assets/Images/carb/blueberries.png'
    },
    {
      id: 10,
      name: 'Broccoli',
      imageUrl: '../../../assets/Images/carb/broccoli.png'
    },
    {
      id: 11,
      name: 'Bulgur',
      imageUrl: '../../../assets/Images/carb/bulgur.png'
    },
    {
      id: 12,
      name: 'Carrot',
      imageUrl: '../../../assets/Images/carb/carrot.png'
    },
    {
      id: 13,
      name: 'Cauliflower',
      imageUrl: '../../../assets/Images/carb/cauliflower.png'
    },
    {
      id: 14,
      name: 'Celery',
      imageUrl: '../../../assets/Images/carb/celery.png'
    },
    {
      id: 15,
      name: 'Chickpeas',
      imageUrl: '../../../assets/Images/carb/chickpeas.png'
    },
    {
      id: 16,
      name: 'Green Chilis',
      imageUrl: '../../../assets/Images/carb/chiligreen.png'
    },
    {
      id: 17,
      name: 'Red Chilis',
      imageUrl: '../../../assets/Images/carb/chilired.png'
    },
    {
      id: 18,
      name: 'Couscous',
      imageUrl: '../../../assets/Images/carb/couscous.png'
    },
    {
      id: 19,
      name: 'Cranberries',
      imageUrl: '../../../assets/Images/carb/cranberries.png'
    },
    {
      id: 20,
      name: 'Cucumber',
      imageUrl: '../../../assets/Images/carb/cucumber.png'
    },
    {
      id: 21,
      name: 'Dark Chocolate',
      imageUrl: '../../../assets/Images/carb/darkchoc.png'
    },
    {
      id: 22,
      name: 'Gooseberries',
      imageUrl: '../../../assets/Images/carb/gooseberries.png'
    },
    {
      id: 23,
      name: 'Grapefruit',
      imageUrl: '../../../assets/Images/carb/grapefruit.png'
    },
    {
      id: 24,
      name: 'Grapes',
      imageUrl: '../../../assets/Images/carb/grapes.png'
    },
    {
      id: 25,
      name: 'Green Beans',
      imageUrl: '../../../assets/Images/carb/greenbeans.png'
    },
    {
      id: 26,
      name: 'Honey',
      imageUrl: '../../../assets/Images/carb/honey.png'
    },
    {
      id: 27,
      name: 'Horseradish',
      imageUrl: '../../../assets/Images/carb/horseradish.png'
    },
    {
      id: 28,
      name: 'Kale',
      imageUrl: '../../../assets/Images/carb/kale.png'
    },
    {
      id: 29,
      name: 'Kidney Beans',
      imageUrl: '../../../assets/Images/carb/kidneybeans.png'
    },
    {
      id: 30,
      name: 'Leek',
      imageUrl: '../../../assets/Images/carb/leek.png'
    },
    {
      id: 31,
      name: 'Lemongrass',
      imageUrl: '../../../assets/Images/carb/lemongrass.png'
    },
    {
      id: 32,
      name: 'Lentils',
      imageUrl: '../../../assets/Images/carb/lentils.png'
    },
    {
      id: 33,
      name: 'Lettuce',
      imageUrl: '../../../assets/Images/carb/lettuce.png'
    },
    {
      id: 34,
      name: 'Limes',
      imageUrl: '../../../assets/Images/carb/limes.png'
    },
    {
      id: 35,
      name: 'Mango',
      imageUrl: '../../../assets/Images/carb/mango.png'
    },
    {
      id: 36,
      name: 'Medjool Dates',
      imageUrl: '../../../assets/Images/carb/medjooldates.png'
    },
    {
      id: 37,
      name: 'Canteloup',
      imageUrl: '../../../assets/Images/carb/meloncanteloup.png'
    },
    {
      id: 38,
      name: 'Honeydew Melon',
      imageUrl: '../../../assets/Images/carb/melonhoneydew.png'
    },
    {
      id: 39,
      name: 'Portabella Mushrooms',
      imageUrl: '../../../assets/Images/carb/mushroomsportabella.png'
    },
    {
      id: 40,
      name: 'Shiitake Muchrooms',
      imageUrl: '../../../assets/Images/carb/mushroomsshiitake.png'
    },
    {
      id: 41,
      name: 'White Mushrooms',
      imageUrl: '../../../assets/Images/carb/mushroomswhite.png'
    },
    {
      id: 42,
      name: 'Mustard',
      imageUrl: '../../../assets/Images/carb/mustard.png'
    },
    {
      id: 43,
      name: 'Nectarine',
      imageUrl: '../../../assets/Images/carb/nectarine.png'
    },
    {
      id: 44,
      name: 'Onions',
      imageUrl: '../../../assets/Images/carb/onions.png'
    },
    {
      id: 45,
      name: 'Oranges',
      imageUrl: '../../../assets/Images/carb/oranges.png'
    },
    {
      id: 46,
      name: 'Papaya',
      imageUrl: '../../../assets/Images/carb/papaya.png'
    },
    {
      id: 47,
      name: 'Parsnips',
      imageUrl: '../../../assets/Images/carb/parsnips.png'
    },
    {
      id: 48,
      name: 'Pasta',
      imageUrl: '../../../assets/Images/carb/pasta.png'
    },
    {
      id: 49,
      name: 'Pears',
      imageUrl: '../../../assets/Images/carb/pears.png'
    },
    {
      id: 50,
      name: 'Peas',
      imageUrl: '../../../assets/Images/carb/peas.png'
    },
    {
      id: 51,
      name: 'Green Peppers',
      imageUrl: '../../../assets/Images/carb/peppergreen.png'
    },
    {
      id: 52,
      name: 'Jalapeno',
      imageUrl: '../../../assets/Images/carb/pepperjalapeno.png'
    },
    {
      id: 53,
      name: 'Red Peppers',
      imageUrl: '../../../assets/Images/carb/pepperred.png'
    },
    {
      id: 54,
      name: 'Yellow Peppers',
      imageUrl: '../../../assets/Images/carb/pepperyellow.png'
    },
    {
      id: 55,
      name: 'Pineapple',
      imageUrl: '../../../assets/Images/carb/pineapple.png'
    },
    {
      id: 56,
      name: 'Potato',
      imageUrl: '../../../assets/Images/carb/potato.png'
    },
    {
      id: 57,
      name: 'Quinoa',
      imageUrl: '../../../assets/Images/carb/quinoa.png'
    },
    {
      id: 58,
      name: 'Raddish',
      imageUrl: '../../../assets/Images/carb/raddish.png'
    },
    {
      id: 59,
      name: 'Rhubarb',
      imageUrl: '../../../assets/Images/carb/rhubarb.png'
    },
    {
      id: 60,
      name: 'Brown Rice',
      imageUrl: '../../../assets/Images/carb/ricebrown.png'
    },
    {
      id: 61,
      name: 'White Rice',
      imageUrl: '../../../assets/Images/carb/ricewhite.png'
    },
    {
      id: 62,
      name: 'Spelt',
      imageUrl: '../../../assets/Images/carb/spelt.png'
    },
    {
      id: 63,
      name: 'Spinach',
      imageUrl: '../../../assets/Images/carb/spinach.png'
    },
    {
      id: 64,
      name: 'Squash',
      imageUrl: '../../../assets/Images/carb/squash.png'
    },
    {
      id: 65,
      name: 'Strawberries',
      imageUrl: '../../../assets/Images/carb/strawberries.png'
    },
    {
      id: 66,
      name: 'Sundried Tomatos',
      imageUrl: '../../../assets/Images/carb/sundriedtomato.png'
    },
    {
      id: 67,
      name: 'Sweet Potato',
      imageUrl: '../../../assets/Images/carb/sweetpotato.png'
    },
    {
      id: 68,
      name: 'Tomato',
      imageUrl: '../../../assets/Images/carb/tomato.png'
    },
    {
      id: 69,
      name: 'Tomato Puree',
      imageUrl: '../../../assets/Images/carb/tomatopuree.png'
    },











  ];

  fatFoods: FatItems [] = [
    {
      id: 1,
      name: 'Avocado',
      imageUrl: '../../../assets/Images/fat/avocado.png'
    },
    {
      id: 2,
      name: 'Buckwheat',
      imageUrl: '../../../assets/Images/fat/buckwheat.png'
    },
    {
      id: 3,
      name: 'Butter',
      imageUrl: '../../../assets/Images/fat/butter.png'
    },
    {
      id: 4,
      name: 'Coconut Oil',
      imageUrl: '../../../assets/Images/fat/coconutoil.png'
    },
    {
      id: 5,
      name: 'Coffee',
      imageUrl: '../../../assets/Images/fat/coffee.png'
    },
    {
      id: 6,
      name: 'Cream',
      imageUrl: '../../../assets/Images/fat/cream.png'
    },
    {
      id: 7,
      name: 'Mayonnaise',
      imageUrl: '../../../assets/Images/fat/mayonnaise.png'
    },
    {
      id: 8,
      name: 'Milk',
      imageUrl: '../../../assets/Images/fat/milk.png'
    },
    {
      id: 9,
      name: 'Almonds',
      imageUrl: '../../../assets/Images/fat/nutsalmonds.png'
    },
    {
      id: 10,
      name: 'Cashews',
      imageUrl: '../../../assets/Images/fat/nutscashews.png'
    },
    {
      id: 11,
      name: 'Peanuts',
      imageUrl: '../../../assets/Images/fat/nutspeanuts.png'
    },
    {
      id: 12,
      name: 'Pistachios',
      imageUrl: '../../../assets/Images/fat/nutspistachios.png'
    },
    {
      id: 13,
      name: 'Walnuts',
      imageUrl: '../../../assets/Images/fat/nutswalnuts.png'
    },
    {
      id: 14,
      name: 'Oats',
      imageUrl: '../../../assets/Images/fat/oats.png'
    },
    {
      id: 15,
      name: 'Olive Oil',
      imageUrl: '../../../assets/Images/fat/oliveoil.png'
    },
    {
      id: 16,
      name: 'Olives',
      imageUrl: '../../../assets/Images/fat/olives.png'
    },
    {
      id: 17,
      name: 'Peanut Butter',
      imageUrl: '../../../assets/Images/fat/peanutbutter.png'
    },
    {
      id: 18,
      name: 'Flax Seed',
      imageUrl: '../../../assets/Images/fat/seedflax.png'
    },
    {
      id: 19,
      name: 'Hemp Seed',
      imageUrl: '../../../assets/Images/fat/seedhemp.png'
    },
    {
      id: 20,
      name: 'Pumpkin Seed',
      imageUrl: '../../../assets/Images/fat/seedpumpkin.png'
    },
    {
      id: 21,
      name: 'Chia Seed',
      imageUrl: '../../../assets/Images/fat/seedschia.png'
    },
    {
      id: 22,
      name: 'Sunflower Seed',
      imageUrl: '../../../assets/Images/fat/seedsunflower.png'
    },
    {
      id: 23,
      name: 'Wheat Bran',
      imageUrl: '../../../assets/Images/fat/wheatbran.png'
    },
    {
      id: 24,
      name: 'Wheatgerm',
      imageUrl: '../../../assets/Images/fat/wheatgerm.png'
    }

  ];

  constructor() { }
}
