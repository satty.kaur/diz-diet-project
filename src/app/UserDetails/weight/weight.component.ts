import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'weight-component',
  templateUrl: './weight.component.html',
  styleUrls: ['./weight.component.css']
})
export class WeightComponent implements OnInit {

  @Output() weightInput = new EventEmitter();

  weightMetric = false;
  stones;
  pounds;
  kg;
  weight;


  constructor() { }

  ngOnInit(): void {
  }


  checkWeight() {
    if (this.stones && this.pounds) {
      this.weight = this.stones + 'st ' + this.pounds + 'lbs';
      this.weightInput.emit(this.weight);
    }
    if (this.kg) {
      this.weight = this.kg + 'kg';
      this.weightInput.emit(this.weight);
    }
  }

}
