import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'gender-component',
  templateUrl: './gender.component.html',
  styleUrls: ['./gender.component.css']
})
export class GenderComponent implements OnInit {

 @Output() genderSelected = new EventEmitter();
 gender;


  constructor() { }

  ngOnInit() {

  }

  genderClick(gen) {
    this.gender = gen;
    this.genderSelected.emit(this.gender);
  }




}
