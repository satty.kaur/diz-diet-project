import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'height-component',
  templateUrl: './height.component.html',
  styleUrls: ['./height.component.css']
})
export class HeightComponent implements OnInit {

  @Output() heightInput = new EventEmitter();

  heightMetric = false;
  height;
  feet;
  inches;
  meter;
  cm;




  constructor() { }

  ngOnInit(): void {


  }

  checkHeight() {


    if (this.feet && this.inches) {

      this.height = this.feet + ' ' + this.inches + '"';
      this.heightInput.emit(this.height);
    }
    if (this.meter && this.cm) {

      this.height = this.meter + '.' + this.cm + 'cm';
      this.heightInput.emit(this.height);
    }
  }

}
export class Height {
  feet?;
  inches?;
  meter?;
  cm?;
}

