import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DietPreferenceComponent } from './diet-preference.component';

describe('DietPreferenceComponent', () => {
  let component: DietPreferenceComponent;
  let fixture: ComponentFixture<DietPreferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietPreferenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DietPreferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
