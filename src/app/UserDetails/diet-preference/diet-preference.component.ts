import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'diet-preference-component',
  templateUrl: './diet-preference.component.html',
  styleUrls: ['./diet-preference.component.css']
})
export class DietPreferenceComponent implements OnInit {

  @Output() dietInput = new EventEmitter();

  selectedDiet;

  constructor() { }

  ngOnInit(): void {
  }

  saveDiet() {
    this.dietInput.emit(this.selectedDiet);
  }

}
