import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'goal-component',
  templateUrl: './goal.component.html',
  styleUrls: ['./goal.component.css']
})
export class GoalComponent implements OnInit {

  @Output() saveGoal = new EventEmitter();

  step = 0;

  items = [];

  days = [];

  months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  years = [];

  percentage = [];



  constructor() { }

  ngOnInit(){
  for (let i = 40; i <= 120; i++) {
    this.items.push(i);
  }

  for (let i = 1; i <= 31; i++) {
      this.days.push(i);
    }

  for (let i = 2020; i <= 2050; i++) {
      this.years.push(i);
  }

  for (let i = 0; i <= 100; i++) {
      this.percentage.push(i);
    }

  }

  saveGoals() {
    this.saveGoal.emit(this.step);
  }

}
