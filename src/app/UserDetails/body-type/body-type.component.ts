import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'body-type-component',
  templateUrl: './body-type.component.html',
  styleUrls: ['./body-type.component.css']
})
export class BodyTypeComponent implements OnInit {

  @Input() gender: string;
  @Output() bodyTypeInput = new EventEmitter();

  bodyType;

  constructor() { }

  ngOnInit(): void {
  }

  bodyTypeClick(body) {
    this.bodyType = body;
    this.bodyTypeInput.emit(this.bodyType);
  }

}
