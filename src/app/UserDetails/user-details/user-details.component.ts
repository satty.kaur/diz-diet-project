import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  step = 1;

  gender;
  age;
  height;

  weight;
  genBodyType;
  bodyType;
  dietPreference;

  getPlans = false;

  constructor() { }

  ngOnInit(): void {
  }

  genderSaved(gen){
    this.gender = gen;
    console.log(this.gender);
    this.genBodyType = this.gender;
    this.step = 2;
  }

  ageSaved(ageSelected) {
    this.age = ageSelected;
    console.log(this.age);
    this.step = 3;
  }

  heightSave(heightSelected) {
    this.height = heightSelected;
    console.log(this.height);
    this.step = 4;
  }

  weightSave(weightSelected) {
    this.weight = weightSelected;
    console.log(this.weight);
    this.step = 5;
  }

  btSave(bt) {
    this.bodyType = bt;
    console.log(this.bodyType);
    this.step = 6;
  }

  dietSave(diet) {
    this.dietPreference = diet;
    console.log(this.dietPreference);
    this.step = 7;
  }

  confirmDetails(value) {
    this.step = value;
    this.getPlans = true;
  }

}
