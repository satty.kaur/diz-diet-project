import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';

@Component({
  selector: 'age-component',
  templateUrl: './age.component.html',
  styleUrls: ['./age.component.css']
})
export class AgeComponent implements OnInit {

  @Output() closed = new EventEmitter();


  age;


  constructor() { }

  ngOnInit(): void {
  }


  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.age = event.value;
    this.closed.emit(this.age);
  }


}
