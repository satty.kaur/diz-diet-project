


$('#slider').roundSlider({
  sliderType: 'min-range',
  circleShape: 'pie',
  startAngle: '315',
  lineCap: 'round',
  radius: 70,
  width: 10,
  mouseScrollAction: true,
  editableTooltip: false,
  min: 0,
  max: 100,
  svgMode: true,
  pathColor: '#eee',
  borderWidth: 0,
  rangeColor: '#29ABE2',
  startValue: 0,
  handleSize: '+6',

});

$('#slider2').roundSlider({
  sliderType: 'min-range',
  circleShape: 'pie',
  startAngle: '315',
  lineCap: 'round',
  radius: 70,
  width: 10,
  mouseScrollAction: true,
  editableTooltip: false,
  min: 0,
  max: 100,
  svgMode: true,
  pathColor: '#eee',
  borderWidth: 0,
  rangeColor: '#009e2a',
  startValue: 0,
  handleSize: '+6',

});

$('#slider3').roundSlider({
  sliderType: 'min-range',
  circleShape: 'pie',
  startAngle: '315',
  lineCap: 'round',
  radius: 70,
  width: 10,
  mouseScrollAction: true,
  editableTooltip: false,
  min: 0,
  max: 100,
  svgMode: true,
  pathColor: '#eee',
  borderWidth: 0,
  rangeColor: '#e39a12',
  startValue: 0,
  handleSize: '+6',

});
