import { Component, OnInit } from '@angular/core';
import {FoodItemsService} from '../FoodData/food-items.service';
import {MatDialog} from '@angular/material/dialog';
import {FoodDialogComponent} from '../food-dialog/food-dialog.component';


@Component({
  selector: 'app-diet-plans',
  templateUrl: './diet-plans.component.html',
  styleUrls: ['./diet-plans.component.css']

})
export class DietPlansComponent implements OnInit {

    foodId: number;
    macro = '';

  constructor(public foodData: FoodItemsService, public dialog: MatDialog) { }

  ngOnInit(): void {

    // @ts-ignore
    $('#slider').roundSlider({
      sliderType: 'min-range',
      circleShape: 'pie',
      startAngle: '315',
      lineCap: 'round',
      radius: 70,
      width: 10,
      mouseScrollAction: true,
      editableTooltip: false,
      min: 0,
      max: 100,
      svgMode: true,
      pathColor: '#eee',
      borderWidth: 0,
      rangeColor: '#29ABE2',
      startValue: 0,
      handleSize: '+6',

    });
    // @ts-ignore
    $('#slider2').roundSlider({
      sliderType: 'min-range',
      circleShape: 'pie',
      startAngle: '315',
      lineCap: 'round',
      radius: 70,
      width: 10,
      mouseScrollAction: true,
      editableTooltip: false,
      min: 0,
      max: 100,
      svgMode: true,
      pathColor: '#eee',
      borderWidth: 0,
      rangeColor: '#009e2a',
      startValue: 0,
      handleSize: '+6',

    });
    // @ts-ignore
    $('#slider3').roundSlider({
      sliderType: 'min-range',
      circleShape: 'pie',
      startAngle: '315',
      lineCap: 'round',
      radius: 70,
      width: 10,
      mouseScrollAction: true,
      editableTooltip: false,
      min: 0,
      max: 100,
      svgMode: true,
      pathColor: '#eee',
      borderWidth: 0,
      rangeColor: '#e39a12',
      startValue: 0,
      handleSize: '+6',

    });

    // @ts-ignore
    $('#slider4').roundSlider({
      sliderType: 'min-range',
      circleShape: 'pie',
      startAngle: '315',
      lineCap: 'round',
      radius: 70,
      width: 10,
      mouseScrollAction: true,
      editableTooltip: false,
      min: 0,
      max: 100,
      svgMode: true,
      pathColor: '#eee',
      borderWidth: 0,
      rangeColor: 'black',
      startValue: 0,
      handleSize: '+6',
    });

    // @ts-ignore
    $('.slider5').roundSlider({
      sliderType: 'min-range',
      circleShape: 'pie',
      startAngle: '315',
      lineCap: 'square',
      radius: 40,
      width: 5,
      mouseScrollAction: true,
      editableTooltip: false,
      min: 0,
      max: 100,
      svgMode: true,
      pathColor: '#eee',
      borderWidth: 0,
      rangeColor: 'LawnGreen',
      startValue: 0,
      handleShape: 'round',
      handleSize: '+2'
    });




  }


  openDialog(id: number, name: string) {
      this.foodId = id;
      this.macro = name;
      const dialogRef = this.dialog.open(FoodDialogComponent, {
        width: '700px',
        data: {id: this.foodId, macro: this.macro}
      });
  }


}


